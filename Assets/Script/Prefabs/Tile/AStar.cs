using System.Collections;
using System.Collections.Generic;
using UnityEngine;



namespace ProjectC
{
    public class Node
    {
        public bool walkable = true;

        public Tile Current;
        public Node parent;
        public int gCost { get; set; }
        public int hCost { get; set; }
        public int fCost => gCost + hCost;
    }

    public class Pathfinding
    {
        public List<Node> nodeList = new List<Node>();


        public void Init(List<Tile> tileList)
        {
            nodeList.Clear();
            foreach (var tile in tileList)
            {
                Node node = new Node();
                node.Current = tile;
                nodeList.Add(node);
            }
        }

        public Node GetNode(int keyIndex)
        {
            return nodeList.Find(obj => obj.Current.keyIndex == keyIndex);
        }

        public Node GetNode(int x, int y)
        {
            var tile = nodeList.Find(obj => obj.Current.pos.x == x && obj.Current.pos.y == y);
            return tile;
        }

        public List<Tile> FindPath(int startKeyIndex, int endKeyIndex)
        {
            var startNode = GetNode(startKeyIndex);
            var targetNode = GetNode(endKeyIndex);

            List<Node> openSet = new List<Node>();
            List<Node> closedSet = new List<Node>();
            List<Tile> pathSet = new List<Tile>();
            openSet.Add(startNode);

            while (openSet.Count > 0)
            {
                Node node = openSet[0];
                for (int i = 1; i < openSet.Count; i++)
                {
                    if (openSet[i].fCost < node.fCost || openSet[i].fCost == node.fCost)
                    {
                        if (openSet[i].hCost < node.hCost)
                            node = openSet[i];
                    }
                }

                openSet.Remove(node);
                if (closedSet.Contains(node) == false)
                {
                    closedSet.Add(node);
                }

                if (node == targetNode)
                {
                    pathSet = RetracePath(startNode, targetNode);

                    closedSet.ForEach((obj) => { obj.Current.Type = Tile.TileType.ATTACK; });
                    pathSet.ForEach((obj) => { obj.Type = Tile.TileType.MOVE; });

                    return pathSet;
                }

                foreach (Node neighbour in GetSearchNearby(node))
                {

                    if (!neighbour.walkable || closedSet.Contains(neighbour) || neighbour.Current.Type == Tile.TileType.BLOCK)
                    {
                        continue;
                    }
                    int newCostToNeighbour = node.gCost + GetDistance(node, neighbour);
                    if (newCostToNeighbour < neighbour.gCost || !openSet.Contains(neighbour))
                    {
                        neighbour.gCost = newCostToNeighbour;
                        neighbour.hCost = GetDistance(neighbour, targetNode);
                        neighbour.parent = node;
                        neighbour.Current.Type = Tile.TileType.ATTACK;

                        if (!openSet.Contains(neighbour))
                            openSet.Add(neighbour);
                    }
                }
            }
            return null;
        }

        List<Tile> RetracePath(Node startNode, Node endNode)
        {
            List<Tile> path = new List<Tile>();
            Node currentNode = endNode;

            while (currentNode != startNode)
            {
                path.Add(currentNode.Current);
                currentNode = currentNode.parent;
            }
            path.Reverse();

            return path;

        }

        int GetDistance(Node nodeA, Node nodeB)
        {
            int dstX = Mathf.Abs(nodeA.Current.pos.x - nodeB.Current.pos.x);
            int dstY = Mathf.Abs(nodeA.Current.pos.y - nodeB.Current.pos.y);

            if (dstX > dstY)
                return 14 * dstY + 10 * (dstX - dstY);
            return 14 * dstX + 10 * (dstY - dstX);
        }

        public List<Node> GetSearchNearby(Node node)
        {
            List<Node> searchNearby = new List<Node>();

            for (int x = -1; x <= 1; x++)
            {
                for (int y = -1; y <= 1; y++)
                {
                    if (x == 0 && y == 0)
                        continue;

                    int checkX = node.Current.pos.x + x;
                    int checkY = node.Current.pos.y + y;

                    if (checkX >= 0 && checkX < BattleManager.Instance.TilesController.tileSize.x && checkY >= 0 && checkY < BattleManager.Instance.TilesController.tileSize.y)
                    {
                        searchNearby.Add(GetNode(checkX, checkY));
                    }
                }
            }
            return searchNearby;
        }



    }


}

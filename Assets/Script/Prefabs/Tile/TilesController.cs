using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace ProjectC
{
    // Tile를 컨트롤하는 클레스
    // 공격 및 이동 위치 찾기
    // 이벤트 만들기
    // 만약 클릭 이벤트가 바뀌면 이벤트 보내기

    public class TilesController : MonoBehaviour
    {
        private const string baseTilePath = "Prefab/Tile/";

        public List<Tile> tileObjectList = new List<Tile>();

        public Vector2Int tileSize = new Vector2Int(20, 20);


        [HideInInspector] public bool isSelectTile = false;
        [HideInInspector] public int targetTileIndex = 0;

        public Pathfinding pathfinding = new Pathfinding();
        public void Awake()
        {
        }

        public void Init()
        {
            for (int y = 0; y < tileSize.y; y++)
            {
                for (int x = 0; x < tileSize.x; x++)
                {
                    //로컬 POS를 오브젝트 생성지 만들어지는걸로
                    GameObject tileObject = ResourceManager.InstantiateAsset($"{baseTilePath}Tile", transform);
                    Tile tile = tileObject.GetComponent<Tile>();
                    tile.Init($"Tile_{x}_{y}"
                      , new Vector3((10 * x), 0.0f, -(10 * y))
                      , (y * tileSize.x) + x
                      , new Vector2Int(x, y));
                    tileObjectList.Add(tile);
                }
            }
            pathfinding.Init(tileObjectList);
            UpdateTile();
        }
        public void Update()
        {
            if (Input.GetMouseButtonDown(0))
            {
                MouseButtonDown(0);

            }
            else if (Input.GetMouseButtonDown(1))
            {
                MouseButtonDown(1);
            }

        }
        public Tile GetTargetTile() => GetTileObj(targetTileIndex);



        public void ButtonEvent(bool isDown)
        {
            if (isDown == true)
            {
            }
        }

        public void MouseButtonDown(int btnType)// 1.LButton 2.RButton
        {
            Vector2 pos = Input.mousePosition;
            Vector3 theTouch = new Vector3(pos.x, pos.y, 0.0f);

            Ray ray = Camera.main.ScreenPointToRay(theTouch);

            RaycastHit hit;
            if (Physics.Raycast(ray, out hit, Mathf.Infinity))
            {
                Tile tile = hit.transform.GetComponent<Tile>();
                if (tile != null)
                {
                    if (btnType == 0) OnLButtonClick(tile);
                    if (btnType == 1) OnRButtonClick(tile);

                }
            }
            UpdateTile();
        }

        private void OnLButtonClick(Tile tile)
        {
            ResetTile();
            if (targetTileIndex == tile.keyIndex && isSelectTile == true)
            {
                isSelectTile = false;
                targetTileIndex = 0;

            }
            else
            {
                isSelectTile = true;
                targetTileIndex = tile.keyIndex;
                EventDispatcher.DispatchEvent("heroActor", targetTileIndex);

            }
        }
        private void OnRButtonClick(Tile tile)
        {
            tile.Type = Tile.TileType.BLOCK;
        }

        public void UpdateTile()
        {
            EventDispatcher.DispatchEvent("UpdateTile", "");
        }

        public Tile GetTileObj(Vector2Int tilePos)
        {
            var tile = tileObjectList.Find(obj => obj.pos.x == tilePos.x && obj.pos.y == tilePos.y);

            return tile;
        }

        public Tile GetTileObj(int keyIndex)
        {
            var tile = tileObjectList.Find(obj => obj.keyIndex == keyIndex);
            return tile;
        }

        private void ResetTile()
        {
            tileObjectList.ForEach((tile) =>
            {
                if (tile.Type != Tile.TileType.BLOCK)
                {
                    tile.Type = Tile.TileType.NORMAL;
                }
            }
            );
        }
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using TMPro;
namespace ProjectC
{
    public class Tile : MonoBehaviour
    {
        public enum TileType
        {
            NORMAL,
            SELECT,
            MOVE,
            ATTACK,
            BLOCK
        }

        [Serializable]
        public class TileInfo
        {
            public TileType type;
            public GameObject tileObject;
        }

        [SerializeField] private BoxCollider rootCollider;
        [SerializeField] private TextMeshPro topText;
        [SerializeField] private TextMeshPro bottomText;


        public TileType Type { get; set; }
        public int keyIndex = 0;
        public Vector2Int pos = new Vector2Int();
        public List<TileInfo> tilePartList = new List<TileInfo>();

        private void Awake()
        {
        }
        public void Init(string name, Vector3 locelPos, int keyIndex, Vector2Int pos)
        {
            transform.localPosition = locelPos;
            this.name = name;
            this.keyIndex = keyIndex;
            this.pos = pos;
            SetTopText();
            SetBottomText();
            EventDispatcher.AddListener("UpdateTile", gameObject, UpdateTile);
        }
        public void SetTopText()
        {
            topText.text = $"Key:{keyIndex}";

        }
        public void SetBottomText(string text = "")
        {
            bottomText.text = $"({pos.x}:{pos.y})\n{text}";

        }
     
        public void UpdateTile(object obj)
        {
            if (BattleManager.Instance.TilesController.isSelectTile == true
                && BattleManager.Instance.TilesController.targetTileIndex == keyIndex)
            {
                SetTileChang(TileType.SELECT);
            }
            else
            {
                SetTileChang(Type);
            }
        }

        private void SetTileChang(TileType type)
        {
            tilePartList.ForEach((tileInfo) => { tileInfo.tileObject.SetActive(tileInfo.type == type); });
        }
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ProjectC;

namespace ProjectC
{
    //SceneBase만들어서 기본적인 부분을 가지고 가자. 예로 : 맵로드와, 카메라 로드, 타일로드의 함수를 베이스에 두자.
    public class SceneBattle : SceneBattleBase
    {
        protected  override void Awake()
        {
            base.Awake();
            Init();
        }

        protected override void Init()
        {
            base.Init();
            BattleManager.Instance.Init();

        }
        //임시 함수



    }
}

using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.SceneManagement;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace ProjectC
{
    public static class ResourceManager
    {
        public static void Log(string text)
        {
            Debug.Log(text);
        }

        #region Manage Asset
#if UNITY_EDITOR
        public static T LoadAssetForEditor<T>(string path) where T : UnityEngine.Object
        {
            Log($"ResourceManager.LoadAssetForEditor(assetPath:{path})");
            return AssetDatabase.LoadAssetAtPath<T>(path);
        }
#endif

        public static T LoadAsset<T>(string path) where T : UnityEngine.Object
        {
            return Resources.Load<T>(path);
        }

        public static ResourceRequest LoadAssetAsync<T>(string path) where T : UnityEngine.Object
        {
            return Resources.LoadAsync<T>(path);
        }

        public static GameObject InstantiateAsset(string path, Transform parent = null)
        {
            return GameObject.Instantiate(LoadAsset<GameObject>(path), Vector3.zero, Quaternion.identity, parent);
        }

        public static IEnumerator InstantiateAssetAsync(string path, Transform parent = null, System.Action<GameObject> onComplete = null)
        {
            var async = LoadAssetAsync<GameObject>(path);
            yield return async;
            onComplete?.Invoke(GameObject.Instantiate(async.asset as GameObject, Vector3.zero, Quaternion.identity, parent));
        }
        #endregion Manage Asset

        #region Manage Scene
        public static void LoadScene(string sceneName, LoadSceneMode loadSceneMode = LoadSceneMode.Single)
        {
            Log($"ResourceManager.LoadScene(sceneName:{sceneName},loadSceneMode:{loadSceneMode})");
            SceneManager.LoadScene(sceneName, loadSceneMode);
        }
        public static AsyncOperation LoadSceneAsync(string sceneName, LoadSceneMode loadSceneMode = LoadSceneMode.Single)
        {
            Log($"ResourceManager.LoadSceneAsync(sceneName:{sceneName},loadSceneMode:{loadSceneMode})");
            return SceneManager.LoadSceneAsync(sceneName, loadSceneMode);
        }
        public static AsyncOperation UnloadSceneAsync(string sceneName)
        {
            return SceneManager.UnloadSceneAsync(sceneName);
        }
        #endregion Manage Scene
    }
}

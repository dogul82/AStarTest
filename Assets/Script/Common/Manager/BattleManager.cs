﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ProjectC
{
    public class BattleManager : SingletonMono<BattleManager>
    {
         private const string basePath = "Prefab/Map/";
         private const string cameraPath = "Prefab/Camera/";

        [SerializeField] private int mapKey;
        [SerializeField] protected Transform cameraRoot;
       
        [HideInInspector] public TilesController TilesController { get; set; }

        protected GameObject battleMap;
        protected GameObject cameraObject;

        public int MapKey => mapKey;
        public Actor heroActor;
       // public List<Actor> actorList = new List<Actor>();

        public void Init()
        {
            EventDispatcher.Instance.Init();
            cameraObject = ResourceManager.InstantiateAsset($"{cameraPath}Camera", cameraRoot.transform);

            battleMap = ResourceManager.InstantiateAsset($"{basePath}{"SampleMap01"}", transform);

            Transform startPoint = battleMap.transform.Find("root").Find("StartPoint");

            startPoint.gameObject.AddComponent<TilesController>();
            TilesController = startPoint.gameObject.GetComponent<TilesController>();
            TilesController.Init();

            heroActor = ActorManager.Instance.Create("ActorSample01");
            heroActor.Move(TilesController.GetTileObj(Vector2Int.zero));
           


        }
        private void Save()
        {


        }
        private void MapSetting()
        {

        }
    }
}

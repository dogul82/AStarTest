using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace ProjectC
{
    public class EventDispatcher : SingletonNew<EventDispatcher>
    {
        public delegate void CallbackFunc(object param);

        private Dictionary<string, List<(GameObject target, CallbackFunc listener)>> eventListeners = new Dictionary<string, List<(GameObject target, CallbackFunc listener)>>();

        public void Init()
        { 
        }
        public static void Clear()
        {
            if (false == InstanceExists())
                return;

            Instance.eventListeners.Clear();
        }

        public static void AddListener(string eventName, GameObject target, CallbackFunc listener)
        {
            if (false == InstanceExists())
                return;

            if(Instance.eventListeners.ContainsKey(eventName))
            {
                Instance.eventListeners[eventName].Add((target, listener));
            }
            else
            {
                Instance.eventListeners.Add(eventName, new List<(GameObject target, CallbackFunc listener)>() { (target, listener) });
            }
        }

        public static void RemoveListener(string eventName, GameObject target)
        {
            if (false == InstanceExists())
                return;

            if(Instance.eventListeners.ContainsKey(eventName))
            {
                var listener = Instance.eventListeners[eventName];
                listener.RemoveAll(x => x.target == target);
            }
        }


        public static void AllRemoveListener(GameObject target)
        {
            if (false == InstanceExists())
                return;

            var enumerator = Instance.eventListeners.GetEnumerator();
            while(enumerator.MoveNext())
            {
                enumerator.Current.Value.RemoveAll(x => x.target == target);
            }
        }

        public static void DispatchEvent(string eventName, object param)
        {
            if (false == InstanceExists())
                return;

            if (Instance.eventListeners.ContainsKey(eventName))
            {
                var listeners = Instance.eventListeners[eventName];
                if (listeners != null)
                {
                    listeners.RemoveAll(x => false == x.target);

                    foreach (var listener in listeners)
                    {
                        listener.listener?.Invoke(param);
                    }
                }
            }
        }
    }
}

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ProjectC
{
    public class ActorManager : SingletonNew<ActorManager>
    {
        private Dictionary<int, Actor> dicActors = new Dictionary<int, Actor>();

        private string actorPath => $"Prefab/Actor/";

        private int generateInstanceKey = 1;
        private int GetGenerateInstanceKey()
        {
            return generateInstanceKey++;
        }

        public Actor Create(string actorName, Transform parent = null)
        {
            var actorModel = ResourceManager.InstantiateAsset(string.Format("{0}{1}", actorPath, actorName), parent);
            if (actorModel is null)
                return null;

            var actor = actorModel.GetComponent<Actor>();
            actor.Init( GetGenerateInstanceKey());
            dicActors.Add(actor.Key, actor);

            return actor;
        }

        public Actor GetActor(int key)
        {
            if (dicActors.ContainsKey(key))
            {
                return dicActors[key];
            }
            return null;
        }

        public Actor GetActor(Vector2Int position)
        {
            foreach (var actor in dicActors)
            {
                if (position.Equals(actor.Value.Position))
                    return actor.Value;
            }

            return null;
        }
        public bool isActor(Vector2Int position)
        {
            return GetActor(position) != null;
        }
    }
}

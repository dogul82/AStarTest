﻿

using UnityEngine;


namespace ProjectC
{
    public abstract class SingletonNew<T> : MonoBehaviour where T : MonoBehaviour
    {
        private static object lockObject = new object();
        private static T instance;

        public static bool InstanceExists()
        {
            return instance;
        }

        public static void DestroyGameObject()
        {
            if (instance)
                Utility.Destroy(instance.gameObject);
        }

        public static T Instance
        {
            get
            {
                lock (lockObject)
                {
                    if (instance == null)
                    {
                        T[] instances = FindObjectsOfType<T>();

                        if (instances != null)
                        {
                            if (instances.Length == 1)
                            {
                                instance = instances[0];
                                return instance;
                            }
                            else if (instances.Length > 1)
                            {
                                Debug.LogError($"You have more than one {typeof(T).Name} in the scene. You only need 1, it's a singleton!");

                                for (int i = 0; i < instances.Length; i++)
                                {
                                    T temp = instances[i];

                                    Utility.Destroy(temp.gameObject);
                                }
                            }
                        }

                        var go = new GameObject(typeof(T).Name, typeof(T));

                        instance = go.GetComponent<T>();
                        go.hideFlags |= (HideFlags.DontSave | HideFlags.DontSaveInBuild);

                        if (Application.isPlaying)
                            DontDestroyOnLoad(go);
                    }

                    return instance;
                }
            }
        }
    }

    public abstract class SingletonMono<T> : MonoBehaviour where T : MonoBehaviour
    {
        private static object lockObject = new object();
        private static T instance;

        // 인스턴스는 왠만하면 그냥 가리자...
        public static T Instance
        {
            get
            {
                lock (lockObject)
                {
                    if (instance == null)
                    {
                        T[] instances = FindObjectsOfType<T>();

                        if (instances != null)
                        {
                            if (instances.Length > 1)
                            {
                                Debug.LogError($"You have more than one {typeof(T).Name} in the scene. You only need 1, it's a singleton!");
                            }
                            if (instances.Length > 0)
                            {
                                instance = instances[0];
                            }
                        }
                    }
                }
                return instance;
            }
        }

        public static bool InstanceExists()
        {
            return instance != null;
        }

        public static void RegisterInstance()
        {
            instance = Instance;
        }
    }

    public abstract class ScriptableInstance<T> : ScriptableObject where T : ScriptableObject
    {
        private static T instance;

        public static bool InstanceExists()
        {
            return instance != null;
        }

        public static void DestroyInstance()
        {
            if (instance)
                Utility.Destroy(instance);
        }

        public static T Instance
        {
            get
            {
                if (instance == null)
                {
                    T[] instances = FindObjectsOfType<T>();

                    if (instances != null)
                    {
                        if (instances.Length == 1)
                        {
                            instance = instances[0];
                            return instance;
                        }
                        else if (instances.Length > 1)
                        {
                            Debug.LogError($"You have more than one {typeof(T).Name} in the scene. You only need 1, it's a singleton!");

                            for (int i = 0; i < instances.Length; i++)
                            {
                                T temp = instances[i];

                                Utility.Destroy(temp);
                            }
                        }
                    }

                    Debug.Log($"CreateInstance - {typeof(T).Name}");

                    instance = CreateInstance<T>();
                }

                return instance;
            }
        }

        protected virtual void OnDestroy()
        {
            Debug.Log($"DestroyInstance - {typeof(T).Name}");
        }

        public static void CleanUp()
        {
            Destroy(instance);
            instance = null;
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.IO;

namespace ProjectC
{
    public static class JsonHelper
    {
        public static void Save<T>(T objectData,string fileName)
        {
            string toJson = ToJson(objectData);// JsonManager.ToJson(userSaveData);
            Debug.Log(toJson);
            string path = GetPath();
            Debug.Log(path);

            DirectoryInfo di = new DirectoryInfo(path);
            if (di.Exists == false)
            {
                di.Create();
            }

            File.WriteAllText($"{GetPath()}{fileName}.json", toJson);

        }
        
        public static T Load<T>( string fileName)
        {
            string jsonString = File.ReadAllText($"{GetPath()}{fileName}.json");
            var objectData  = FromJson<T>(jsonString);
            return objectData ;
        }
        private static string GetPath()
        {
            return $"{Application.persistentDataPath}/Saves/";
        }

        //FromJson
        public static T FromJson<T>(string json)
        {
            Wrapper<T> wrapper = UnityEngine.JsonUtility.FromJson<Wrapper<T>>(json);
            return wrapper.item;
        }

        public static List<T> FromJaonLiat<T>(string json)
        {
            WrapperList<T> wrapper = UnityEngine.JsonUtility.FromJson<WrapperList<T>>(json);
            return wrapper.items;
        }
   
        //ToSjon        ​
        public static string ToJson<T>(T json)
        {
            Wrapper<T> wrapper = new Wrapper<T>();
            wrapper.item = json;
            return UnityEngine.JsonUtility.ToJson(wrapper);
        }

        public static string ToJsonList<T>(List<T> array)
        {
            WrapperList<T> wrapper = new WrapperList<T>();
            wrapper.items = array;
            return UnityEngine.JsonUtility.ToJson(wrapper);
        }

        public static string ToJson<T>(T data, bool prettyPrint)
        {
            Wrapper<T> wrapper = new Wrapper<T>();
            wrapper.item = data;
            return UnityEngine.JsonUtility.ToJson(wrapper, prettyPrint);
        }

        public static string ToJsonList<T>(List<T> datas, bool prettyPrint)
        {
            WrapperList<T> wrapper = new WrapperList<T>();
            wrapper.items = datas;
            return UnityEngine.JsonUtility.ToJson(wrapper, prettyPrint);
        }
        [Serializable]
        private class Wrapper<T>
        {
            public T item;
        }

        [Serializable]
        private class WrapperList<T>
        {
            public List<T> items;
        }
    }
}
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ProjectC
{
    
    public static class Utility
    {
        public static void Destroy(Object obj, bool immediate = false)
        {
            if (false == obj)
                return;

            if (immediate || false == Application.isPlaying)
                Object.DestroyImmediate(obj);
            else
                Object.Destroy(obj);
        }
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace ProjectC
{
    public class Actor : MonoBehaviour
    {
        public int Key { get; private set; }

        public int Team { get; set; } = 0;
        public Vector2Int Position { get; set; }

        private Coroutine moveCoroutine;
        public void Init(int key)
        {
            Key = key;
            EventDispatcher.AddListener("heroActor", gameObject, OnMove);

        }

        public void OnMove(object path)
        {
            var actor = BattleManager.Instance.TilesController.GetTileObj(Position);
            var pathList = BattleManager.Instance.TilesController.pathfinding.FindPath(actor.keyIndex, (int)path);
            if (moveCoroutine != null)
            {
                StopCoroutine(moveCoroutine);
                moveCoroutine = null;
            }
            moveCoroutine = StartCoroutine(CoActorMove(pathList));
        }
        IEnumerator CoActorMove(List<Tile> pathList)
        {
            foreach (var tile in pathList)
            {
                Move(tile);
                yield return new WaitForSeconds(0.5f);
            }
            yield return null;
        }
        public void Move(int x, int y)
        {
            Vector2Int pos = new Vector2Int(x, y);
            var tile = BattleManager.Instance.TilesController.GetTileObj(pos);
            Move(tile);
        }

        public void Move(Tile tile)
        {
            Position = tile.pos;
            transform.position = tile.transform.position;
        }
    }
}